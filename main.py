import sys
import os

sys.path.append(os.getcwd() + "/gcp_lib")
# print(sys.path)

from collections import namedtuple
from datetime import datetime
from typing import Callable, Union, Iterable
import json
from pprint import pprint
from zoneinfo import ZoneInfo

# print(*sys.path, sep="\n")
# print("-" * 20)

import pandas as pd
from google.cloud import pubsub_v1


from gcp_lib.gsheet import GSheet

# from gcp_lib.gsheet import Boolean

subscriber = pubsub_v1.SubscriberClient()


def get_pubsub_message_sync(project_id: str, subscription_id: str) -> list:
    # subscriber.subscription_path(project_id, subscription_id)  # somehow this
    # doesn't work
    subscription_path = "projects/cn-ops-spdigital/subscriptions/e2c-korea-poc"
    print("Listening for messages..\n")

    # not sure why pubsub only reading 20 messages, so retry until pubsub read
    # more than 50 msg
    import time

    print("hi")
    while True:
        response = subscriber.pull(
            request={
                "subscription": subscription_path,
                "max_messages": 2000,
            }
        )
        messages = [msg for msg in response.received_messages]
        print(len(messages))
        if len(messages) >= 50:
            break
        time.sleep(5)

    print(
        """
        %s: Received %s messages.
        """
        % (datetime.now().strftime("%H:%M:%S"), len(messages))
        # len(response.received_messages))
    )

    if not messages:
        raise ValueError("No message received from PubSub!")

    return messages, subscriber, subscription_path


def get_pubsub_message(project_id: str, subscription_id: str) -> list:
    """fetch messages from pubsub

    Args:
        project_id (str)
        subscription_id (str)

    Returns:
        list: list of message for consumption
    """
    messages = []

    def callback(message):
        # print(message)
        messages.append(message)

    # subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(project_id, subscription_id)

    future = subscriber.subscribe(
        subscription_path, callback=callback, await_callbacks_on_shutdown=True
    )

    print("Listening for messages..\n")

    try:
        future.result(timeout=5)
    except Exception as e:
        # the exception handling is weird, but it seems future.result() need to
        # be to be called again after cancel() to avoid half-complete message error
        # https://github.com/googleapis/python-pubsub/issues/176
        future.cancel()
        future.result()

    print(
        """
        %s: Received %s messages.
        """
        % (datetime.now().strftime("%H:%M:%S"), len(messages))
        # len(response.received_messages))
    )

    if not messages:
        raise ValueError("No message received from PubSub!")

    return messages, subscriber, subscription_path


def e2c_transformation(pubsub_df: pd.DataFrame, config: Iterable) -> pd.DataFrame:
    """transformation logic for e2c. flow:
        1. Read existing gsheet as df
        2. concat above with incoming new pubsub df
        3. filter away expired time period in dataframe
        4. filter away duplicate primary key

    Args:
        pubsub_df (pd.DataFrame)
        config (Iterable)
    Returns:
        pd.DataFrame: for writing back to gsheet
    """
    c = config

    gs = GSheet.from_secret_manager(
        project_id="cn-ops-spdigital", secret_id="composer", version="1"
    )
    existing_gsheet_df = gs.get_as_df(c.spreadsheet_id, c.sheet_name)
    # print((existing_gsheet_df))
    if (
        len(existing_gsheet_df) > 0
    ):  # if empty dataframe because of empty google sheet for some reason
        existing_gsheet_df = existing_gsheet_df.rename(
            columns={existing_gsheet_df.columns[0]: "time"}
        )  # range A1 has been used as last_update field

    df = pd.concat([existing_gsheet_df, pubsub_df], ignore_index=True)

    # remove out of bound records
    # df['time'] = df['time'].str.replace('-08-02T', '-08-09T') # dev only: pretend to be today
    df["time"] = pd.to_datetime(df["time"], utc=True)
    df["time_delta"] = pd.Timestamp.now(tz="UTC") - df["time"]

    # target_minutes = kwargs["target_minutes"]
    within_n_min = df["time_delta"] <= pd.Timedelta(minutes=c.target_minutes)
    df = df[within_n_min]

    # remove duplicate base on primary key
    # turn to list for pandas consumption
    pk_cols = c.pk_cols.replace(" ", "").split(",")
    df = df.drop_duplicates(pk_cols)

    # sort columns base on config,
    # sort_cols = kwargs["sort_cols"]
    _unzipped_tuple = [*zip(*c.sort_cols)]  # unzip list of tuple
    # turn tuple to list for pandas consumption
    _unzipped_lst = [*map(list, _unzipped_tuple)]
    _sort_by, _sort_ascending = _unzipped_lst[0], _unzipped_lst[1]
    df = df.sort_values(by=_sort_by, ascending=_sort_ascending)

    # display last_update time on gsheet
    # gsheet_timezone = kwargs["gsheet_timezone"]
    tz_now = datetime.now(ZoneInfo(c.gsheet_timezone))
    dt_part = tz_now.strftime("%H:%M:%S")
    tz_part = datetime.isoformat(tz_now)[-6:]

    timecol = f"last update at {dt_part}\n{tz_part} ({c.gsheet_timezone})"  # should this provide TZ info for user?
    df = df.rename(columns={"time": timecol})

    # remove helper column from output
    del df["time_delta"]

    return df


def transform(
    messages: pubsub_v1.subscriber.message.Message,
    logic_func: Callable,
    config: Iterable,
) -> pd.DataFrame:
    """convert pubsub messages to dataframe, perform transformation with generic function

    Args:
        messages (pubsub_v1.subscriber.message.Message): [description]
        logic_func (function): generic function to perform transformation, signatures must expect incoming dataframe and a config, at the end return a dataframe 
        config: config parameter to be used in transformation, can be any type depends on logic_func

    Returns:
        [type]: [description]
    """ """

    Args:
        msg ([type]): [description]

    Returns:
        [type]: [description]
    """

    # convert pubsub message from byte to dict
    # data = [json.loads(m.data) for m in messages]
    data = [json.loads(m.message.data) for m in messages]
    pubsub_df = pd.DataFrame(data)
    transformed_df = logic_func(pubsub_df, config)

    return transformed_df


def main(response=None):
    # import os
    # print(os.environ) # TODO: descide env: local or in cloud

    # This suppose to read from somewhere by DAG
    transform_config = {
        "project_id": "cn-ops-spdigital",
        "secret_id": "composer",
        "version": 1,
        "subscription_id": "e2c-korea-poc",
        "spreadsheet_id": "https://docs.google.com/spreadsheets/d/1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus/edit#gid=0",
        "sheet_name": "data",
        "target_minutes": 120,
        "pk_cols": "measurement, time",
        "sort_cols": [("time", False), ("measurement", True)],
        "gsheet_timezone": "Asia/Seoul",
    }

    # unpack config dict into named tuple for easy access
    c = namedtuple("config", transform_config)(**transform_config)

    # fetch message from pubsub, subscriber for end of operation message ack
    # messages, subscriber, subscription_path = get_pubsub_message(
    #     c.project_id, c.subscription_id
    # )
    messages, subscriber, subscription_path = get_pubsub_message_sync(
        c.project_id, c.subscription_id
    )

    # generic transform by supplying transform logic function and config used in transform
    df = transform(messages, e2c_transformation, c)

    # write transformed df back to gsheet
    gs = GSheet.from_secret_manager(
        project_id="cn-ops-spdigital", secret_id="composer", version="1"
    )
    ws = gs.get_as_sheet(c.spreadsheet_id, c.sheet_name)
    gs.df_to_gsheet(df, ws)
    # print(df[df.columns[0]].unique())
    # ack all messages after data is pasted to gsheet
    # for m in messages:
    #     print(m)
    #     m.ack()
    ack_ids = [m.ack_id for m in messages]

    subscriber.acknowledge(
        request={"subscription": subscription_path, "ack_ids": ack_ids}
    )
    subscriber.close()

    return ("", 200)


if __name__ == "__main__":
    # main()
    # This suppose to read from somewhere by DAG
    transform_config = {
        "project_id": "cn-ops-spdigital",
        "secret_id": "composer",
        "version": 1,
        "subscription_id": "e2c-korea-poc",
        "spreadsheet_id": "https://docs.google.com/spreadsheets/d/1-CErwAPkuS9Vi_-olJtB6GyxxqMSHs703y5JMqgblus/edit#gid=0",
        "sheet_name": "data",
        "target_minutes": 120,
        "pk_cols": "measurement, time",
        "sort_cols": [("time", False), ("measurement", True)],
        "gsheet_timezone": "Asia/Seoul",
    }

    # unpack config dict into named tuple for easy access
    c = namedtuple("config", transform_config)(**transform_config)

    # fetch message from pubsub, subscriber for end of operation message ack
    # messages, subscriber, subscription_path = get_pubsub_message(
    #     c.project_id, c.subscription_id
    # )
    messages, subscriber, subscription_path = get_pubsub_message_sync(
        c.project_id, c.subscription_id
    )
